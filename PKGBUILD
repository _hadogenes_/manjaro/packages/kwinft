# Maintainer: Bernhard Landauer <bernhard@manjaro.org>
# Maintainer: Philip Müller <philm@manjaro.org>
# Author: Roman Gilg <subdiff@gmail.com>
# Contributor: Antonio Rojas <arojas@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>

_pkgname=kwin
pkgname=kwinft
pkgver=5.19.1
pkgrel=1
pkgdesc='drop-in replacement for KWin with additional libwayland wrapping Qt/C++ library Wrapland'
arch=(x86_64)
url="https://gitlab.com/$pkgname/$pkgname"
license=(LGPL)
depends=(breeze
    kdisplay
    kinit
    kscreenlocker
    plasma-framework
    qt5-script
    wrapland
    xcb-util-cursor)
makedepends=(extra-cmake-modules
    kdoctools
    qt5-tools)
optdepends=('qt5-virtualkeyboard: virtual keyboard support for kwin-wayland')
provides=("$_pkgname")
conflicts=("$_pkgname")
source=("$url/-/archive/$pkgname@$pkgver/$pkgname-$pkgname@$pkgver.tar.gz")
sha256sums=('efb1f9d51e4a41da71fdeeedcf4e81414cc8cc92a983d353fe5470bae9de27d4')
install=$pkgname.install

prepare() {
  mkdir -p build
}

build() {
  cd build
  cmake ../$pkgname-$pkgname@$pkgver \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DCMAKE_INSTALL_LIBEXECDIR=lib \
    -DBUILD_TESTING=OFF
  make
}

package() {
  cd build
  make DESTDIR="$pkgdir" install
}
